COMMON_-_END = Beenden
COMMON_-_CANCEL = Abbrechen
COMMON_-_SAVE_AND_EXIT = Speichern & Beenden
COMMON_-_BACK = Zurück
COMMON_-_CONTINUE = Weiter
COMMON_-_WEEKDAY_-_0 = Sonntag
COMMON_-_WEEKDAY_-_1 = Montag
COMMON_-_WEEKDAY_-_2 = Dienstag
COMMON_-_WEEKDAY_-_3 = Mittwoch
COMMON_-_WEEKDAY_-_4 = Donnerstag
COMMON_-_WEEKDAY_-_5 = Freitag
COMMON_-_WEEKDAY_-_6 = Samstag
COMMON_-_COUNTRY_CODES_-_AF = Afghanistan
COMMON_-_COUNTRY_CODES_-_AL = Albanien
COMMON_-_COUNTRY_CODES_-_DZ = Algerien
COMMON_-_COUNTRY_CODES_-_AS = Samoa
COMMON_-_COUNTRY_CODES_-_AD = Andorra
COMMON_-_COUNTRY_CODES_-_AO = Angola
COMMON_-_COUNTRY_CODES_-_AI = Anguilla
COMMON_-_COUNTRY_CODES_-_AQ = Antarktis
COMMON_-_COUNTRY_CODES_-_AG = Antigua und Barbuda
COMMON_-_COUNTRY_CODES_-_AR = Argentinien
COMMON_-_COUNTRY_CODES_-_AM = Armenien
COMMON_-_COUNTRY_CODES_-_AW = Aruba
COMMON_-_COUNTRY_CODES_-_AU = Australien
COMMON_-_COUNTRY_CODES_-_AT = Österreich
COMMON_-_COUNTRY_CODES_-_AZ = Aserbaidschan
COMMON_-_COUNTRY_CODES_-_BS = Bahamas
COMMON_-_COUNTRY_CODES_-_BH = Bahrain
COMMON_-_COUNTRY_CODES_-_BD = Bangladesh
COMMON_-_COUNTRY_CODES_-_BB = Barbados
COMMON_-_COUNTRY_CODES_-_BY = Weissrussland
COMMON_-_COUNTRY_CODES_-_BE = Belgien
COMMON_-_COUNTRY_CODES_-_BZ = Belize
COMMON_-_COUNTRY_CODES_-_BJ = Benin
COMMON_-_COUNTRY_CODES_-_BM = Bermudas
COMMON_-_COUNTRY_CODES_-_BT = Bhutan
COMMON_-_COUNTRY_CODES_-_BO = Bolivien
COMMON_-_COUNTRY_CODES_-_BA = Bosnien-Herzegowina
COMMON_-_COUNTRY_CODES_-_BW = Botswana
COMMON_-_COUNTRY_CODES_-_BV = Bouvet Inseln
COMMON_-_COUNTRY_CODES_-_BR = Brasilien
COMMON_-_COUNTRY_CODES_-_IO = Britisch-Indischer Ozean
COMMON_-_COUNTRY_CODES_-_BN = Brunei
COMMON_-_COUNTRY_CODES_-_BG = Bulgarien
COMMON_-_COUNTRY_CODES_-_BF = Burkina Faso
COMMON_-_COUNTRY_CODES_-_BI = Burundi
COMMON_-_COUNTRY_CODES_-_KH = Kambodscha
COMMON_-_COUNTRY_CODES_-_CM = Kamerun
COMMON_-_COUNTRY_CODES_-_CA = Kanada
COMMON_-_COUNTRY_CODES_-_CV = Kap Verde
COMMON_-_COUNTRY_CODES_-_KY = Kaiman Inseln
COMMON_-_COUNTRY_CODES_-_CF = Zentralafrikanische Republik
COMMON_-_COUNTRY_CODES_-_TD = Tschad
COMMON_-_COUNTRY_CODES_-_CL = Chile
COMMON_-_COUNTRY_CODES_-_CN = China
COMMON_-_COUNTRY_CODES_-_CX = Christmas Island
COMMON_-_COUNTRY_CODES_-_CC = Kokosinseln
COMMON_-_COUNTRY_CODES_-_CO = Kolumbien
COMMON_-_COUNTRY_CODES_-_KM = Komoren
COMMON_-_COUNTRY_CODES_-_CG = Kongo
COMMON_-_COUNTRY_CODES_-_CD = Demokratische Republik Kongo
COMMON_-_COUNTRY_CODES_-_CK = Cook Inseln
COMMON_-_COUNTRY_CODES_-_CR = Costa Rica
COMMON_-_COUNTRY_CODES_-_CI = Elfenbeinküste
COMMON_-_COUNTRY_CODES_-_HR = Kroatien
COMMON_-_COUNTRY_CODES_-_CU = Kuba
COMMON_-_COUNTRY_CODES_-_CY = Zypern
COMMON_-_COUNTRY_CODES_-_CZ = Tschechische Republik
COMMON_-_COUNTRY_CODES_-_DK = Dänemark
COMMON_-_COUNTRY_CODES_-_DJ = Djibuti
COMMON_-_COUNTRY_CODES_-_DM = Dominika
COMMON_-_COUNTRY_CODES_-_DO = Dominikanische Republik
COMMON_-_COUNTRY_CODES_-_EC = Ecuador
COMMON_-_COUNTRY_CODES_-_EG = Ägypten
COMMON_-_COUNTRY_CODES_-_SV = El Salvador
COMMON_-_COUNTRY_CODES_-_GQ = Äquatorial Guinea
COMMON_-_COUNTRY_CODES_-_ER = Eritrea
COMMON_-_COUNTRY_CODES_-_EE = Estland
COMMON_-_COUNTRY_CODES_-_ET = Äthiopien
COMMON_-_COUNTRY_CODES_-_FK = Falkland Inseln
COMMON_-_COUNTRY_CODES_-_FO = Färöer Inseln
COMMON_-_COUNTRY_CODES_-_FJ = Fidschi
COMMON_-_COUNTRY_CODES_-_FI = Finnland
COMMON_-_COUNTRY_CODES_-_FR = Frankreich
COMMON_-_COUNTRY_CODES_-_GF = französisch Guyana
COMMON_-_COUNTRY_CODES_-_PF = Französisch Polynesien
COMMON_-_COUNTRY_CODES_-_TF = Französisches Süd-Territorium
COMMON_-_COUNTRY_CODES_-_GA = Gabun
COMMON_-_COUNTRY_CODES_-_GM = Gambia
COMMON_-_COUNTRY_CODES_-_GE = Georgien
COMMON_-_COUNTRY_CODES_-_DE = Deutschland
COMMON_-_COUNTRY_CODES_-_GH = Ghana
COMMON_-_COUNTRY_CODES_-_GI = Gibraltar
COMMON_-_COUNTRY_CODES_-_GR = Griechenland
COMMON_-_COUNTRY_CODES_-_GL = Grönland
COMMON_-_COUNTRY_CODES_-_GD = Grenada
COMMON_-_COUNTRY_CODES_-_GP = Guadeloupe
COMMON_-_COUNTRY_CODES_-_GU = Guam
COMMON_-_COUNTRY_CODES_-_GT = Guatemala
COMMON_-_COUNTRY_CODES_-_GG = Guernsey
COMMON_-_COUNTRY_CODES_-_GN = Guinea
COMMON_-_COUNTRY_CODES_-_GW = Guinea Bissau
COMMON_-_COUNTRY_CODES_-_GY = Guyana
COMMON_-_COUNTRY_CODES_-_HT = Haiti
COMMON_-_COUNTRY_CODES_-_HM = Heard und McDonald Islands
COMMON_-_COUNTRY_CODES_-_VA = Vatikan
COMMON_-_COUNTRY_CODES_-_HN = Honduras
COMMON_-_COUNTRY_CODES_-_HK = Hong Kong
COMMON_-_COUNTRY_CODES_-_HU = Ungarn
COMMON_-_COUNTRY_CODES_-_IS = Island
COMMON_-_COUNTRY_CODES_-_IN = Indien
COMMON_-_COUNTRY_CODES_-_ID = Indonesien
COMMON_-_COUNTRY_CODES_-_IR = Iran
COMMON_-_COUNTRY_CODES_-_IQ = Irak
COMMON_-_COUNTRY_CODES_-_IE = Irland
COMMON_-_COUNTRY_CODES_-_IM = Isle of Man
COMMON_-_COUNTRY_CODES_-_IL = Israel
COMMON_-_COUNTRY_CODES_-_IT = Italien
COMMON_-_COUNTRY_CODES_-_JM = Jamaika
COMMON_-_COUNTRY_CODES_-_JP = Japan
COMMON_-_COUNTRY_CODES_-_JE = Jersey
COMMON_-_COUNTRY_CODES_-_JO = Jordanien
COMMON_-_COUNTRY_CODES_-_KZ = Kasachstan
COMMON_-_COUNTRY_CODES_-_KE = Kenia
COMMON_-_COUNTRY_CODES_-_KI = Kiribati
COMMON_-_COUNTRY_CODES_-_KP = Nord Korea
COMMON_-_COUNTRY_CODES_-_KR = Süd Korea
COMMON_-_COUNTRY_CODES_-_XK = Kosovo
COMMON_-_COUNTRY_CODES_-_KW = Kuwait
COMMON_-_COUNTRY_CODES_-_KG = Kirgisistan
COMMON_-_COUNTRY_CODES_-_LA = Laos
COMMON_-_COUNTRY_CODES_-_LV = Lettland
COMMON_-_COUNTRY_CODES_-_LB = Libanon
COMMON_-_COUNTRY_CODES_-_LS = Lesotho
COMMON_-_COUNTRY_CODES_-_LR = Liberia
COMMON_-_COUNTRY_CODES_-_LY = Libyen
COMMON_-_COUNTRY_CODES_-_LI = Liechtenstein
COMMON_-_COUNTRY_CODES_-_LT = Litauen
COMMON_-_COUNTRY_CODES_-_LU = Luxemburg
COMMON_-_COUNTRY_CODES_-_MO = Macao
COMMON_-_COUNTRY_CODES_-_MK = Mazedonien
COMMON_-_COUNTRY_CODES_-_MG = Madagaskar
COMMON_-_COUNTRY_CODES_-_MW = Malawi
COMMON_-_COUNTRY_CODES_-_MY = Malaysia
COMMON_-_COUNTRY_CODES_-_MV = Malediven
COMMON_-_COUNTRY_CODES_-_ML = Mali
COMMON_-_COUNTRY_CODES_-_MT = Malta
COMMON_-_COUNTRY_CODES_-_MH = Marshall Inseln
COMMON_-_COUNTRY_CODES_-_MQ = Martinique
COMMON_-_COUNTRY_CODES_-_MR = Mauretanien
COMMON_-_COUNTRY_CODES_-_MU = Mauritius
COMMON_-_COUNTRY_CODES_-_YT = Mayotte
COMMON_-_COUNTRY_CODES_-_MX = Mexiko
COMMON_-_COUNTRY_CODES_-_FM = Mikronesien
COMMON_-_COUNTRY_CODES_-_MD = Moldavien
COMMON_-_COUNTRY_CODES_-_MC = Monaco
COMMON_-_COUNTRY_CODES_-_MN = Mongolei
COMMON_-_COUNTRY_CODES_-_MS = Montserrat
COMMON_-_COUNTRY_CODES_-_MA = Marokko
COMMON_-_COUNTRY_CODES_-_MZ = Mocambique
COMMON_-_COUNTRY_CODES_-_MM = Birma
COMMON_-_COUNTRY_CODES_-_NA = Namibia
COMMON_-_COUNTRY_CODES_-_NR = Nauru
COMMON_-_COUNTRY_CODES_-_NP = Nepal
COMMON_-_COUNTRY_CODES_-_NL = Niederlande
COMMON_-_COUNTRY_CODES_-_AN = Niederländische Antillen
COMMON_-_COUNTRY_CODES_-_NC = Neukaledonien
COMMON_-_COUNTRY_CODES_-_NZ = Neuseeland
COMMON_-_COUNTRY_CODES_-_NI = Nicaragua
COMMON_-_COUNTRY_CODES_-_NE = Niger
COMMON_-_COUNTRY_CODES_-_NG = Nigeria
COMMON_-_COUNTRY_CODES_-_NU = Niue
COMMON_-_COUNTRY_CODES_-_NF = Norfolk Inseln
COMMON_-_COUNTRY_CODES_-_MP = Marianen
COMMON_-_COUNTRY_CODES_-_NO = Norwegen
COMMON_-_COUNTRY_CODES_-_OM = Oman
COMMON_-_COUNTRY_CODES_-_PK = Pakistan
COMMON_-_COUNTRY_CODES_-_PW = Palau
COMMON_-_COUNTRY_CODES_-_PS = Palästina
COMMON_-_COUNTRY_CODES_-_PA = Panama
COMMON_-_COUNTRY_CODES_-_PG = Papua Neuguinea
COMMON_-_COUNTRY_CODES_-_PY = Paraguay
COMMON_-_COUNTRY_CODES_-_PE = Peru
COMMON_-_COUNTRY_CODES_-_PH = Philippinen
COMMON_-_COUNTRY_CODES_-_PN = Pitcairn
COMMON_-_COUNTRY_CODES_-_PL = Polen
COMMON_-_COUNTRY_CODES_-_PT = Portugal
COMMON_-_COUNTRY_CODES_-_PR = Puerto Rico
COMMON_-_COUNTRY_CODES_-_QA = Qatar
COMMON_-_COUNTRY_CODES_-_RE = Reunion
COMMON_-_COUNTRY_CODES_-_RO = Rumänien
COMMON_-_COUNTRY_CODES_-_RU = Russland
COMMON_-_COUNTRY_CODES_-_RW = Ruanda
COMMON_-_COUNTRY_CODES_-_SH = St. Helena
COMMON_-_COUNTRY_CODES_-_KN = St. Kitts Nevis Anguilla
COMMON_-_COUNTRY_CODES_-_LC = Saint Lucia
COMMON_-_COUNTRY_CODES_-_PM = St. Pierre und Miquelon
COMMON_-_COUNTRY_CODES_-_VC = St. Vincent
COMMON_-_COUNTRY_CODES_-_WS = Samoa
COMMON_-_COUNTRY_CODES_-_SM = San Marino
COMMON_-_COUNTRY_CODES_-_ST = Sao Tome
COMMON_-_COUNTRY_CODES_-_SA = Saudi Arabien
COMMON_-_COUNTRY_CODES_-_SN = Senegal
COMMON_-_COUNTRY_CODES_-_RS = Serbien
COMMON_-_COUNTRY_CODES_-_ME = Montenegro
COMMON_-_COUNTRY_CODES_-_SC = Seychellen
COMMON_-_COUNTRY_CODES_-_SL = Sierra Leone
COMMON_-_COUNTRY_CODES_-_SG = Singapur
COMMON_-_COUNTRY_CODES_-_SK = Slowakei
COMMON_-_COUNTRY_CODES_-_SI = Slowenien
COMMON_-_COUNTRY_CODES_-_SB = Solomon Inseln
COMMON_-_COUNTRY_CODES_-_SO = Somalia
COMMON_-_COUNTRY_CODES_-_ZA = Südafrika
COMMON_-_COUNTRY_CODES_-_GS = Südgeorgien und die Südlichen Sandwichinseln
COMMON_-_COUNTRY_CODES_-_ES = Spanien
COMMON_-_COUNTRY_CODES_-_LK = Sri Lanka
COMMON_-_COUNTRY_CODES_-_SD = Sudan
COMMON_-_COUNTRY_CODES_-_SR = Surinam
COMMON_-_COUNTRY_CODES_-_SJ = Svalbard und Jan Mayen Islands
COMMON_-_COUNTRY_CODES_-_SZ = Swasiland
COMMON_-_COUNTRY_CODES_-_SE = Schweden
COMMON_-_COUNTRY_CODES_-_CH = Schweiz
COMMON_-_COUNTRY_CODES_-_SY = Syrien
COMMON_-_COUNTRY_CODES_-_TW = Taiwan
COMMON_-_COUNTRY_CODES_-_TJ = Tadschikistan
COMMON_-_COUNTRY_CODES_-_TZ = Tansania
COMMON_-_COUNTRY_CODES_-_TH = Thailand
COMMON_-_COUNTRY_CODES_-_TL = Osttimor
COMMON_-_COUNTRY_CODES_-_TG = Togo
COMMON_-_COUNTRY_CODES_-_TK = Tokelau
COMMON_-_COUNTRY_CODES_-_TO = Tonga
COMMON_-_COUNTRY_CODES_-_TT = Trinidad Tobago
COMMON_-_COUNTRY_CODES_-_TN = Tunesien
COMMON_-_COUNTRY_CODES_-_TR = Türkei
COMMON_-_COUNTRY_CODES_-_TM = Turkmenistan
COMMON_-_COUNTRY_CODES_-_TC = Turks und Kaikos Inseln
COMMON_-_COUNTRY_CODES_-_TV = Tuvalu
COMMON_-_COUNTRY_CODES_-_UG = Uganda
COMMON_-_COUNTRY_CODES_-_UA = Ukraine
COMMON_-_COUNTRY_CODES_-_AE = Vereinigte Arabische Emirate
COMMON_-_COUNTRY_CODES_-_GB = Großbritannien (UK)
COMMON_-_COUNTRY_CODES_-_US = Vereinigte Staaten von Amerika
COMMON_-_COUNTRY_CODES_-_UY = Uruguay
COMMON_-_COUNTRY_CODES_-_UZ = Usbekistan
COMMON_-_COUNTRY_CODES_-_VU = Vanuatu
COMMON_-_COUNTRY_CODES_-_VE = Venezuela
COMMON_-_COUNTRY_CODES_-_VN = Vietnam
COMMON_-_COUNTRY_CODES_-_VG = Virgin Island (Brit.)
COMMON_-_COUNTRY_CODES_-_VI = Virgin Island (USA)
COMMON_-_COUNTRY_CODES_-_WF = Wallis et Futuna
COMMON_-_COUNTRY_CODES_-_EH = Westsahara
COMMON_-_COUNTRY_CODES_-_YE = Jemen
COMMON_-_COUNTRY_CODES_-_ZM = Sambia
COMMON_-_COUNTRY_CODES_-_ZW = Zimbabwe
ERRORS_-_VALIDATION_URL = Bitte trage eine korrekte URL ein
ERRORS_-_VALIDATION_AGB = Bitte stimme unseren AGB zu
ERRORS_-_VALIDATION_CODE = Bitte gib einen Code ein.
ERRORS_-_VALIDATION_EMAIL = Bitte gib eine gültige Mailadresse ein.
ERRORS_-_VALIDATION_PASSWORD = Bitte gib ein Passwort ein.
ERRORS_-_VALIDATION_ALREADY_REGISTERED = E-Mail ist bereits angemeldet:
ERRORS_-_COGNITO_-_CodeMismatchException = Ungültiger Code. Bitte versuche es erneut.
ERRORS_-_COGNITO_-_ExpiredCodeException = Code ist abgelaufen.
ERRORS_-_COGNITO_-_LimitExceededException = Limit an versuchen erreicht. Bitte versuche es später noch einmal.
ERRORS_-_COGNITO_-_NotAuthorizedException = E-Mail und Passwort stimmen nicht überein.
LOGIN_-_LANGUAGE = DE
LOGIN_-_PASSWORD = Passwort
LOGIN_-_SIGN_IN = Login
LOGIN_-_SIGN_UP = Registrieren
LOGIN_-_CONFIRM_SIGN_UP = Los geht's
LOGIN_-_RESEND_CODE = Code erneut senden
LOGIN_-_VERIFY_CAPTION = Verifizieren
LOGIN_-_VERIFY_HEADLINE = Check deine Mails
LOGIN_-_VERIFY_TEXT_PART_ONE = Wir haben dir einen 6-stelligen Code an
LOGIN_-_VERIFY_TEXT_PART_TWO = gesendet. Lass dieses Fenster offen, während du deine E-Mails abrufst. Gib danach deinen Code und dein neues Passwort in die entsprechenden Felder ein:
LOGIN_-_VERIFY_TEXT_PART_THREE = gesendet. Lass dieses Fenster offen, während du deine E-Mails abrufst. Gib danach deinen Code in dieses Feld ein:
LOGIN_-_VERIFY_HINT = Du findest keine E-Mail? Warte am besten noch ein paar Minuten und prüfe auch deinen Spam-Ordner.
LOGIN_-_CODE = Code
LOGIN_-_NEW_PASSWORD = Neues Passwort
LOGIN_-_PASSWORD_HINT = 8 Zeichen oder mehr
LOGIN_-_RESET_PASSWORD = Passwort zurücksetzen
LOGIN_-_REMEMBER_PASSWORD = Passwort doch nicht vergessen?
LOGIN_-_FORGOT_PASSWORD = Passwort vergessen?
LOGIN_-_FORGOT_PASSWORD_CAPTION = Log In
LOGIN_-_FORGOT_PASSWORD_HEADLINE = Passwort vergessen
LOGIN_-_FORGOT_PASSWORD_TEXT = Gib die E-Mail-Adresse ein, mit der du dich bei uns registriert hast, wir schicken dir eine Mail um dein Passwort zurückzusetzen.
LOGIN_-_EMAIL = E-Mail
LOGIN_-_SIGN_IN_CAPTION = Log In
LOGIN_-_SIGN_IN_HEADLINE = Willkommen zurück
LOGIN_-_SIGN_IN_SUBMIT = Anmelden
LOGIN_-_SIGN_UP_CAPTION = Sign Up
LOGIN_-_SIGN_UP_HEADLINE = Admin werden
LOGIN_-_CONFIRM_AGB = Hiermit erkläre ich mich mit dem Allgemeinen Geschäftsbedingungen einverstanden.
LOGIN_-_CONFIRM_NEWSLETTER = Ja, ich möchte Informationen zu Komm Kickern per E-Mail erhalten.
LOGIN_-_SIGN_UP_SUBMIT = Account erstellen
LOGIN_-_LOGIN_INFO_-_0_-_TITLE = Locations
LOGIN_-_LOGIN_INFO_-_0_-_TEXT_HEADER = Verwalte deine Location
LOGIN_-_LOGIN_INFO_-_0_-_TEXT = Mit der Plattform für Admins, kannst du selbst bestimmen wie deine Location aussehen soll. Beschreibung, Spielinfos, Tische, schicke Fotos uvm.
LOGIN_-_LOGIN_INFO_-_0_-_IMAGE = kki-host-locations-de.png
LOGIN_-_LOGIN_INFO_-_1_-_TITLE = Turniere
LOGIN_-_LOGIN_INFO_-_1_-_TEXT_HEADER = Erstelle Turniere & lass dich finden
LOGIN_-_LOGIN_INFO_-_1_-_TEXT = Als Admin kannst du Turniere für deine Locations anlegen. Unsere Nutzer:innen können diese in der App finden und mit ihren Freunden teilen.
LOGIN_-_LOGIN_INFO_-_1_-_IMAGE = kki-host-turniere-de.png
LOGIN_-_LOGIN_INFO_-_2_-_TITLE = Turnierserie
LOGIN_-_LOGIN_INFO_-_2_-_TEXT_HEADER = Ranglisten und lokale Helden
LOGIN_-_LOGIN_INFO_-_2_-_TEXT = Bekomme Ranglisten für deine Location und nimm automatisch an der Turnierserie Teil. Spieler:innen können sich miteinander Messen und sogar Lokalmatador:innen werden.
LOGIN_-_LOGIN_INFO_-_2_-_IMAGE = kki-host-turnierserie-de.png
LOGIN_-_ACCEPT_PRIVACY_POLICY = Mit Klick auf "Account erstellen" stimme ich zu, dass ich die [Datenschutzbedingungen](https://komm-kickern.de/privacy/) gelesen habe und akzeptiere.
ACCOUNT_-_CONTACT = Kontaktdaten
ACCOUNT_-_FIRST_NAME = Vorname
ACCOUNT_-_LAST_NAME = Nachname
ACCOUNT_-_BTN_CHANGE_CONTACT = Kontaktdaten ändern
ACCOUNT_-_PASSORD = Passwort
ACCOUNT_-_PASSWORD_OLD = Passwort alt
ACCOUNT_-_PASSWORD_NEW = Passwort neu
ACCOUNT_-_BTN_CHANGE_PASSWORD = Passwort ändern
LOCATION_-_HEAD_-_BASE_INFO = Basis Informationen
LOCATION_-_HEAD_-_WEBSITE_SOCIAL = Webseite & Social Media
LOCATION_-_HEAD_-_ADDITIONAL_INFOS = Weitere Infos
LOCATION_-_HEAD_-_OPENING = Öffnungszeiten
LOCATION_-_HEAD_-_DESCRIPTION = Beschreibungen
LOCATION_-_HEAD_-_TABLES = Tische
LOCATION_-_HEAD_-_PHOTOS = Fotos
LOCATION_-_DESCRIPTION_-_HEAD = Über die Location
LOCATION_-_DESCRIPTION_-_DESCRIPTION = Beschreibung
LOCATION_-_DESCRIPTION_-_TEXT = Was gibt es noch wichtiges zu wissen? Habt ihr besondere Hausregeln? Ist euer Eingang schwer zu finden? Habt ihr Live-Musik oder Pub-Quizzes? Seid ihr eher rockig oder poppig? Gibt es bestimmte Getränke oder gar keine?
LOCATION_-_DESCRIPTION_-_FOOSBALL_DESCRIPTION = Spielinfos
LOCATION_-_DESCRIPTION_-_FOOSBALL_DESCRIPTION_TEXT = Hier kannst du konkrete Infos zum Kickern hinterlassen. Interessant sind zum Beispiel regelmäßige Trainings oder die durchschnittliche Spielstärke eurer Gäste.
LOCATION_-_ADDITIONAL_INFOS_-_HEAD = Weitere Infos
LOCATION_-_ADDITIONAL_INFOS_-_WHEELCHAIR = Rollstuhlgerecht
LOCATION_-_ADDITIONAL_INFOS_-_WHEELCHAIR_TEXT = Menschen im Rollstuhl können deine Räumlichkeiten ohne besondere Erschwernis und grundsätzlich ohne fremde Hilfe betreten. Auch die Toiletten sind mit Rollstuhl erreich- und nutzbar.
LOCATION_-_ADDITIONAL_INFOS_-_SMOKING = Raucher-Location
LOCATION_-_ADDITIONAL_INFOS_-_SMOKING_TEXT = Manche Menschen lieben Raucherkneipen, andere hassen sie. Lass deine Gäste vorher wissen, was sie erwartet. Hierbei geht es um den Kickerbereich: Steht der Kicker in einem Raucherbereich? Dann aktiviere das Feld.
LOCATION_-_BASE_INFO_-_HEAD = Basis Informationen
LOCATION_-_BASE_INFO_-_NAME = Name
LOCATION_-_BASE_INFO_-_ADDRESS = Adresse
LOCATION_-_BASE_INFO_-_STREET = Straße
LOCATION_-_BASE_INFO_-_POSTAL_CODE = Postleitzahl
LOCATION_-_BASE_INFO_-_CITY = Stadt
LOCATION_-_BASE_INFO_-_CONTACT = Kontakt
LOCATION_-_BASE_INFO_-_PHONE = Telefonnummer
LOCATION_-_BASE_INFO_-_LOCATION_KIND = Art der Location
LOCATION_-_BASE_INFO_-_KIND = Art
LOCATION_-_PHOTOS_-_HEAD = Fotos
LOCATION_-_PHOTOS_-_TEXT = Deine Fotos werden in der App in der selben Reihenfolge angezeigt wie hier. Du kannst sie per Drag and Drop oder Nutzung der Pfeile in die gewünschte Reihenfolge bringen.
LOCATION_-_PHOTOS_-_DELETE = Löschen?
LOCATION_-_PHOTOS_-_SECONDS_REMAINING = {remainingSeconds} Sekunden verbleibend
LOCATION_-_PHOTOS_-_WAIT = Warten ...
LOCATION_-_PHOTOS_-_UPLOAD_HINT = Fotos hierher ziehen oder [Ordner durchsuchen](javascript:void(0))
LOCATION_-_PHOTOS_-_UPLOAD_SIZE_HINT = Max. Dateigröße: 20MB • Dateiformat: JPEG, PNG, WEBP
LOCATION_-_PHOTOS_-_ADDITIONAL_INFOS_TEXT = Bevor du deine Fotos hochlädst, sieh dir diese | Hinweise | zum verwenden von Fotografien an.
LOCATION_-_WEBSITE_SOCIAL_-_HEAD = Website & Social Media
LOCATION_-_WEBSITE_SOCIAL_-_WEBSITE = Webseite
LOCATION_-_WEBSITE_SOCIAL_-_TWITTER = Twitter
LOCATION_-_WEBSITE_SOCIAL_-_FACEBOOK = Facebook
LOCATION_-_WEBSITE_SOCIAL_-_V_KONTAKTE = vKontakte
LOCATION_-_WEBSITE_SOCIAL_-_INSTAGRAM = Instagram
LOCATION_-_LIST_-_NO_LOCATION_HEAD = Lege deine erste Location an
LOCATION_-_LIST_-_NO_LOCATION_TEXT = Um Turniere für deine Location planen zu können, möchten wir erst wissen, welche Location dir gehört.
    
    Wenn die Location bereits in unserer Datenbank ist, verknüpfen wir diese damit. Auf diese Weise kannst du immer dafür Sorgen, dass all deine Daten aktuell sind.
LOCATION_-_LIST_-_NO_LOCATION_BTN = Location anlegen
LOCATION_-_LIST_-_BTN_ADD_LOCATION = Location hinzufügen
LOCATION_-_LIST_-_OPENING = Öffnungszeiten
LOCATION_-_LIST_-_DESCRIPTION = Beschreibung
LOCATION_-_LIST_-_TABLES = Tische
LOCATION_-_LIST_-_PHOTOS = Fotos
LOCATION_-_OPENING_-_HEAD = Öffnungszeiten
LOCATION_-_OPENING_-_WEEKDAY = Wochentag
LOCATION_-_OPENING_-_OPEN = Offen
LOCATION_-_OPENING_-_OPEN24 = 24 Std
LOCATION_-_OPENING_-_WITH_REGISTRATION_ONLY = Nur mit Anmeldung
LOCATION_-_OPENING_-_WITH_REGISTRATION_ONLY_TEXT = Solltet ihr keine festen Öffnungszeiten haben, aber eure Tore für Besucher:innen mit Vorabanmeldung öffnen, kannst du diese Option wählen.
LOCATION_-_SEARCH_-_HEAD = Suche nach deiner Location
LOCATION_-_SEARCH_-_TEXT = Suche deine Location mit Hilfe von Google Maps. Auf diese Weise können wir sicherstellen, dass wir die Location an der richtigen Stelle anzeigen. Außerdem können wir in unserer Datenbank prüfen, ob es deine Location vielleicht auch schon gibt und verknüpfen.
LOCATION_-_SEARCH_-_LOCATION = Location
LOCATION_-_SEARCH_-_PLACEHOLDER = Adresse oder Namen eingeben
LOCATION_-_SEARCH_-_HAS_OWNER_HEAD = Location bereits vergeben
LOCATION_-_SEARCH_-_HAS_OWNER_TEASER = Oh Nein! Jemand anderes hat sich bereits als Inhaber:in für diese Location eingetragen.
LOCATION_-_SEARCH_-_HAS_OWNER_TEXT = Bitte schreibe uns eine E-Mail an [info@komm-kickern.de](mailto:info@komm-kickern.de) und wir werden uns so schnell wie möglich bei dir melden. 
    
    Leider kannst du an dieser Stelle nicht weiter machen. Das tut uns sehr leid. Aber wir sind uns sicher, dass sich alles lösen lässt.
LOCATION_-_SEARCH_-_LOCATION_EXISTS_HEAD = Location in unserer Datenbank gefunden
LOCATION_-_SEARCH_-_LOCATION_EXISTS_TEASER = Hey, wir haben deine Location in unserer Datenbank gefunden!
LOCATION_-_SEARCH_-_LOCATION_EXISTS_TEXT = Schön, dass du da bist. Wir tragen dich jetzt als Inhaber:in ein und deine Location wird in der App als verifiziert angezeigt. 
    
    In den nächsten Schritten werden wir noch ein paar relevante Informationen abfragen. Da deine Location bereits bei uns registriert ist, kann es gut sein, dass du schon viele vorausgefüllte Felder finden wirst. Klicke einfach auf “Weiter” und dann kann es losgehen.
LOCATION_-_SEARCH_-_LOCATION_NEW_HEAD = Neue Location angelegt
LOCATION_-_SEARCH_-_LOCATION_NEW_TEASER = Wie schön! Wir kannten deine Location noch gar nicht und haben sie nun angelegt. Willkommen!
LOCATION_-_SEARCH_-_LOCATION_NEW_TEXT = Wir tragen dich jetzt als Inhaber:in ein und deine Location wird in der App als verifiziert angezeigt. In den nächsten Schritten werden wir noch ein paar relevante Informationen abfragen. 
    
    Klicke einfach auf “Weiter” und dann kann es losgehen.
LOCATION_-_TABLES_-_HEAD = Tische
LOCATION_-_TABLES_-_CONDITION = Zustand
LOCATION_-_TABLES_-_COUNT = Anzahl
LOCATION_-_TABLES_-_PAID = Bezahltisch
LOCATION_-_TABLES_-_BTN_ADD_TABLE = + Tisch hinzufügen
LOCATION_-_CLOSE_MODAL_-_HEAD = Location anlegen beenden
LOCATION_-_CLOSE_MODAL_-_WHAT = Was möchtest du tun?
LOCATION_-_CLOSE_MODAL_-_WHAT_TEXT = Möchtest du alle bisherigen ausgfüllten Felder zu deiner Location Speichern und zu einem anderen Zeitpunkt weiter machen?
LOCATION_-_CLOSE_MODAL_-_BTN_SAVE_EXIT = Speichern & Beenden
LOCATION_-_CLOSE_MODAL_-_OR_DELETE = Oder möchtest du, dass wir alles Löschen?
LOCATION_-_CLOSE_MODAL_-_DELETE_EXIT = Löschen & Beenden
LOCATION_-_CLOSE_MODAL_-_NICE_LOCATION = Was für eine schöne Location!
LOCATION_-_CLOSE_MODAL_-_NICE_LOCATION_TEXT = Cool, dass du soviel eingetragen hast. Wir werden jetzt alles Speichern und in der App anzeigen. Du kannst jederzeit deine Location bearbeiten.
LOCATION_-_CLOSE_MODAL_-_LOOKS_GOOD = Sieht doch schon mal gut aus!
LOCATION_-_CLOSE_MODAL_-_LOOKS_GOOD_TEXT = Wir werden jetzt alles Speichern und in der App anzeigen. Du kannst jederzeit deine Location bearbeiten und noch fehlende Informationen nachtragen.
LOCATION_-_PHOTO_INFO_MODAL_-_HEAD = Hinweise zum Fotoupload
LOCATION_-_PHOTO_INFO_MODAL_-_TEXT = Lade Fotos von deiner Location hoch, damit unsere Nutzer:innen wissen worauf sie sich freuen können. Für einen besseren Eindruck hilft es, nicht nur den Kickertische zu zeigen. Wie sieht die Bar aus? Wo steht der Kicker? Wie sieht die Location von außen aus.
    
    Achte bitte darauf, dass du keine Persönlichkeits- oder Uhrheberrechte verletzt. Verwende eigene Fotos oder hole dir ein Einverständnis. Am besten sind Fotos ohne Menschen. Es ist auch okay, wenn die Gesichter unkenntlich sind oder die Fotografierten nur “Beiwerk” - sie stehen also nicht im Mittelpunkt des Bildes.
LOCATION_-_PHOTO_INFO_MODAL_-_TABLES = Kickertisch
LOCATION_-_PHOTO_INFO_MODAL_-_TABLES_TEXT = Auch wenn das erste Foto schick ist: Wie ein Kickertisch aussieht wissen wir. Viel interessanter ist: Wie sieht der Kickerraum aus? Wieviel Platz ist dort? Wieviele Kicker stehen dort?
LOCATION_-_PHOTO_INFO_MODAL_-_PEOPLE = Menschen
LOCATION_-_PHOTO_INFO_MODAL_-_PEOPLE_TEXT = Wenn Menschen im Mittelpunkt des Fotos stehen, holt euch bitte ein Einverständnis, dass ihr diese Fotos verwenden dürft. Sind die Leute unkenntlich oder nur “Beiwerk” ist es okay.
LOCATION_-_PHOTO_INFO_MODAL_-_INTERESTING = Interessante Bilder
LOCATION_-_PHOTO_INFO_MODAL_-_INTERESTING_TEXT = Nicht nur euer Kickertisch ist interessant. Zeigt auch gern eure Fassade, Bar, Flipper, Logo etc.
NAVIGATION_-_DASHBOARD = Dashboard
NAVIGATION_-_TOURNAMENTS = Turniere
NAVIGATION_-_LOCATION = Location
NAVIGATION_-_ACCOUNT = Account
NAVIGATION_-_LOGOUT = Logout
PICKUP_-_HEAD = Willkommen!<br/>Schön, dass du da bist.
PICKUP_-_WHAT_NEXT = Was möchtest du als nächstes tun?
PICKUP_-_CREATE_NEW_LOCATION = Meine Location erstellen.
PICKUP_-_LOOK_AROUND = Ich schaue mich erstmal um.
TOURNAMENTS_-_STEP_1 = Schritt 1
TOURNAMENTS_-_STEP_1_HEAD = Location anlegen
TOURNAMENTS_-_STEP_1_TEXT = Um Turniere für eine Location anlegen zu können müssen wir erst wissen, welche Location dir gehört.
TOURNAMENTS_-_STEP_1_QUESTION = Deine Location gibt es bereits in der Komm Kickern App?
TOURNAMENTS_-_STEP_1_ANSWER = Um so besser, weniger Arbeit für dich, denn dann wirst du im Anlege-Prozess vorausgefüllte Felder finden. Alles ist verknüpft.
TOURNAMENTS_-_STEP_2 = Schritt 2
TOURNAMENTS_-_STEP_2_HEAD = Admin App laden
TOURNAMENTS_-_STEP_2_TEXT = Die Turnierverwaltung läuft über die “Komm Kickern Admin” App. Lade die App und logge dich mit deiner Login-ID ein. Diese findest du auf dem Home-Screen.
TOURNAMENTS_-_STEP_2_QUESTION = Warum extra über eine App und nicht hier?
TOURNAMENTS_-_STEP_2_ANSWER = Weil wir nicht alles auf einmal geschafft haben. Eine App bleibt einfach praktischer. Egal wo, das Smartphone ist immer dabei. Schritt für Schritt werden wir aber auch hier mehr Funktionen zur Verfügung stellen.
TOURNAMENTS_-_STEP_3 = Schritt 3
TOURNAMENTS_-_STEP_3_HEAD = Turnier anlegen
TOURNAMENTS_-_STEP_3_TEXT = Lege ein Turnier zum gewünschten Zeitpunkt an. Dieses wird nun in der Komm Kickern App öffentlich angezeigt und Spieler:innen können dein Turnier finden.
TOURNAMENTS_-_STEP_3_QUESTION = Und nun?
TOURNAMENTS_-_STEP_3_ANSWER = Eine Stunde vor Turnierstart öffnet sich die Anmeldung. Dann kannst du den QR-Code für den Check-in vorzeigen. 
    
    Du musst dann nur noch das KO starten, wenn ihr soweit seit. Kein Sorge, auch das wird bald automatisiert, dann hast du gar keine Arbeit mehr. 😉
DASHBOARD_-_NEXT_STEPS = Deine nächsten<br/>Schritte
DASHBOARD_-_ADD_LOCATION = Location anlegen
DASHBOARD_-_GET_APP = App laden & einchecken
DASHBOARD_-_CREATE_TOURNAMENT = Erstes Turnier anlegen
DASHBOARD_-_WELCOME_TEXT = ### Hallo, 
    
    das hier wird dein Dashboard, sobald du ein paar Turniere gespielt hast, wirst du hier Statistiken zu deinen Turnieren und Rankings der Spieler:innen in deiner Location finden. 
    
    Solltest du noch Fragen haben, schau doch mal in unserem FAQ vorbei oder schreibe uns einfach eine Mail. Auch über Feedback freuen wir uns.
DASHBOARD_-_FAQ_LINK = https://komm-kickern.de/faq/
DASHBOARD_-_MAIL_LINK = mailto:info@komm-kickern.de
DASHBOARD_-_LOGIN_ID = Login-ID
DASHBOARD_-_LOGIN_ID_PLACEHOLDER = Sobald du deine Location angelegt hast findest du hier den Login für die Admin App.
DASHBOARD_-_SERIES = Turnierserie
DASHBOARD_-_SERIES_APP_NAME = Komm Kickern Admin App
DASHBOARD_-_SERIES_TEXT = Lege als Admin öffentliche KOMM KICKERN-Turniere für deine Location an und werde Turnierleiter:in ohne selbst mitzuspielen.
DASHBOARD_-_SERIES_HINT = *Alle Turniere über die Admin App gehören zur Turnierserie. Rankings folgen ab Oktober.
